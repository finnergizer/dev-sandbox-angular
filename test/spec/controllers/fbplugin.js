'use strict';

describe('Controller: FbpluginCtrl', function () {

  // load the controller's module
  beforeEach(module('devPlaygroundApp'));

  var FbpluginCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    FbpluginCtrl = $controller('FbpluginCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
