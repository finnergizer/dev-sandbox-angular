'use strict';

/**
 * @ngdoc function
 * @name fbEventPluginApp.controller:NavCtrl
 * @description
 * # NavCtrl
 * Controller of the fbEventPluginApp
 */
angular.module('devPlaygroundApp')
  .controller('NavCtrl', function ($scope, $location) {
    $scope.isActive = function (viewLocation) {
      return viewLocation === $location.path();
    };
  });
