'use strict';

/**
 * @ngdoc overview
 * @name devPlaygroundApp
 * @description
 * # devPlaygroundApp
 *
 * Main module of the application.
 */
angular
  .module('devPlaygroundApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/fbPlugin', {
        templateUrl: 'views/fbplugin.html',
        controller: 'FbpluginCtrl'
      })
      .when('/contact', {
        templateUrl: 'views/contact.html',
        controller: 'ContactCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
